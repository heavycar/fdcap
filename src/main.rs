#![feature(async_await)]

use once_cell::sync::Lazy;
use failure::format_err;
use futures::{
    compat::{Future01CompatExt, Stream01CompatExt},
    prelude::*,
};
use log::{error, info, warn};
use std::{
    env,
    fs,
    io::{self, BufReader, BufWriter, Write},
    net::{IpAddr, Ipv4Addr, SocketAddr, TcpListener},
    path::{Path, PathBuf},
    process,
};
use structopt::StructOpt;

type FResult<T = ()> = Result<T, failure::Error>;

/// Log every running match's framedump data.
#[derive(StructOpt)]
#[structopt(author = "")]
struct Opt {
    /// Path to framedump output directory
    ///
    /// Defaults to "Matches" in `../TAGame/Logs`.
    #[structopt(short, long)]
    out: Option<PathBuf>,
    /// Port to listen on.
    #[structopt(short, long, default_value = "50000")]
    port: u16,
    /// Use async IO.
    #[structopt(short, long = "async")]
    asynchronous: bool,
    #[structopt(short, long, parse(from_occurrences))]
    verbose: u8,
    /// Hide all log output except for errors.
    #[structopt(short, long)]
    silent: bool,
}

static LOG_DIR: Lazy<PathBuf> = Lazy::new(|| {
    PathBuf::from(env::var("USERPROFILE").expect("Failed to find home directory."))
        .join(r"Documents\My Games\Rocket League\TAGame\Logs\Matches")
});

fn run() -> FResult {
    let opt: Opt = Opt::from_args();

    let log_level = if opt.silent {
        simplelog::LevelFilter::Error
    } else {
        match opt.verbose {
            0 => simplelog::LevelFilter::Info,
            1 => simplelog::LevelFilter::Debug,
            _ => simplelog::LevelFilter::Trace,
        }
    };
    simplelog::TermLogger::init(log_level, simplelog::Config::default())?;

    //    let out_path = opt.out.unwrap_or_else(|| LOG_DIR.to_owned());
    let out_path = match opt.out {
        Some(ref p) => p.clone(),
        None => LOG_DIR.to_owned(),
    };

    if !out_path.is_dir() {
        std::fs::create_dir_all(&out_path)?;
    }

    //    log_framedumps(&out_path, opt.port)?;

    if opt.asynchronous {
        tokio::run(
            async move {
                if let Err(e) = log_framedumps_async(&out_path, opt.port).await {
                    error!("{}", e);
                }
            }
                .unit_error()
                .boxed()
                .compat(),
        );
    } else {
        log_framedumps(&out_path, opt.port)?;
    }

    info!("Done");
    Ok(())
}

async fn log_framedumps_async(directory: &Path, port: u16) -> FResult {
    let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), port);
    let server = tokio::net::TcpListener::bind(&addr)?;
    let mut incoming = server.incoming().compat();
    info!("Listening on port {}", port);

    while let Some(connection) = incoming.next().await {
        info!("Incoming connection");
        match connection {
            Ok(connection) => {
                info!("Successfully connected");
                let mut connection: tokio::net::TcpStream = connection;

                let date = chrono::Utc::now();
                let date_string = date.to_rfc3339_opts(chrono::SecondsFormat::Secs, true);
                let date_string = date_string.replace(':', "-");
                let filename = directory.join(format!("{}-framedump.bin.log", date_string));
                info!("Writing to: {}", filename.to_string_lossy());
                let mut file = tokio::fs::File::create(filename).compat().await?;

                if let Err(e) = tokio::io::copy(&mut connection, &mut file).compat().await {
                    warn!("Copy error: {}", e);
                    file.flush()?;
                }
            }
            Err(e) => warn!("Failed to connect: {}", e),
        }
    }

    Ok(())
}

fn log_framedumps(directory: &Path, port: u16) -> FResult {
    let addr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), port);
    let server = TcpListener::bind(addr)?;

    crossbeam::scope(|scope| -> FResult {
        let mut threads = vec![];

        info!("Listening on port {}", port);
        for connection in server.incoming() {
            info!("Incoming connection");
            match connection {
                Ok(connection) => {
                    info!("Successfully connected");
                    let handle = scope.spawn(move |_| {
                        if let Err(e) = (|| -> FResult {
                            let mut connection = BufReader::new(connection);
                            let date = chrono::Utc::now();
                            let date_string =
                                date.to_rfc3339_opts(chrono::SecondsFormat::Secs, true);
                            let date_string = date_string.replace(':', "-");
                            let filename =
                                directory.join(format!("{}-framedump.bin.log", date_string));
                            info!("Writing to: {}", filename.to_string_lossy());
                            let file = fs::File::create(&filename)?;
                            let mut file = BufWriter::new(file);

                            if let Err(e) = io::copy(&mut connection, &mut file) {
                                warn!("Copy error: {}", e);
                                file.flush()?;
                            }

                            Ok(())
                        })() {
                            error!("{}", e);
                        }
                    });
                    threads.push(handle);
                }
                Err(e) => warn!("Failed to connect: {}", e),
            }
        }

        Ok(())
    })
    .map_err(|_| format_err!("Crossbeam error"))?
}

fn main() {
    let res = match std::panic::catch_unwind(run) {
        Ok(r) => r,
        Err(e) => {
            error!("Panic: {:?}", e);
            process::exit(1);
        }
    };

    if let Err(e) = res {
        error!("{}", e);
        for cause in e.iter_causes() {
            error!("Cause:\n\t{}", cause);
        }
        let code = e
            .downcast_ref::<io::Error>()
            .and_then(io::Error::raw_os_error)
            .unwrap_or(1);
        process::exit(code);
    }
}
