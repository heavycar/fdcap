# fdcap

```
fdcap
Log every running match's framedump data.

USAGE:
    fdcap [FLAGS] [OPTIONS]

FLAGS:
    -a, --async      
            Use async IO.

    -h, --help       
            Prints help information

    -s, --silent     
            Hide all log output except for errors.

    -V, --version    
            Prints version information

    -v, --verbose    
            


OPTIONS:
    -o, --out <out>      
            Path to framedump output directory
            
            Defaults to "Matches" in `../TAGame/Logs`.
    -p, --port <port>    
            Port to listen on. [default: 50000]
```

## Building

```bash
cargo build --release
```